package event

type Event struct {
	Name string
	// todo: Payload as byte slice or as a type of its own
	Payload string
}
