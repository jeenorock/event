package event

import (
	"log"
	"time"
)

const defaultTimeout = 30 * time.Second

type ConcurrentDispatcher struct {
	dispatcher SimpleDispatcher
	timeout    *time.Duration
}

func NewConcurrentDispatcher(timeout *time.Duration) ConcurrentDispatcher {
	if timeout == nil {
		t := defaultTimeout
		timeout = &t
	}

	return ConcurrentDispatcher{
		dispatcher: NewSimpleDispatcher(),
		timeout:    timeout,
	}
}

func (d *ConcurrentDispatcher) Publish(e Event) []error {
	log.Printf("Concurrent dispatching event %s\n", e.Name)

	var errors []error

	handlers := d.dispatcher.getHandlers(e.Name)
	activeHandlers := len(handlers)

	errStream := make(chan error)
	abortSignal := make(chan interface{})
	defer func() {
		log.Println("Closing errStream")
		close(errStream)
	}()

	for _, handler := range handlers {
		go func(handler Handler, errPort chan<- error, abortPort <-chan interface{}) {
			err := handler.Handle(e)

			select {
			case <-abortPort:
				log.Println("Handler aborting")
				return
			default:
				log.Println("Handler sending to errPort")
				errPort <- err
			}
		}(handler, errStream, abortSignal)
	}

	// we create the timeout here instead of doing it in the select to avoid resetting at every iteration
	timeoutSignal := time.After(*d.timeout)

	for {
		select {
		case ret := <-errStream:
			activeHandlers--
			if ret != nil {
				errors = append(errors, ret)
			}

			// todo: solve timeout issue
			// if we timeout the routines are still running: they will try to write in a closed channel
		case <-timeoutSignal:
			log.Println("Dispatcher timeout")
			activeHandlers = 0 // force break
			close(abortSignal)
		}

		if activeHandlers <= 0 {
			break
		}
	}

	return errors
}

func (d *ConcurrentDispatcher) Subscribe(eventName string, handler Handler) int {
	return d.dispatcher.Subscribe(eventName, handler)
}
