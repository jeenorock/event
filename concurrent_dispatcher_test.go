package event

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

func TestConcurrentDispatcher_Publish(t *testing.T) {
	e := Event{Name: "test"}
	d := NewConcurrentDispatcher(nil)
	initHandlers(10, &d, e)
	d.Publish(e)
}

func TestConcurrentDispatcher_PublishTimeout(t *testing.T) {
	timeout := 5 * time.Second
	d := NewConcurrentDispatcher(&timeout)
	e := Event{Name: "test"}

	for i := 0; i < 10; i++ {
		d.Subscribe(e.Name, TimeoutHandler{
			Timeout: time.Duration(i) * time.Second,
			Prefix:  fmt.Sprintf("%v", i),
		})
		d.Subscribe(e.Name, TimeoutHandler{
			Timeout: time.Duration(i) * time.Second,
			Prefix:  fmt.Sprintf("%v", i),
		})
	}

	d.Publish(e)

	// default timeout
	d2 := NewConcurrentDispatcher(nil)
	for i := 0; i < 10; i++ {
		d2.Subscribe(e.Name, TimeoutHandler{
			Timeout: time.Duration(i) * time.Second,
			Prefix:  fmt.Sprintf("%v", i),
		})
	}

	d2.Publish(e)
}

func BenchmarkConcurrentDispatcher_Publish(b *testing.B) {
	e := Event{Name: "test"}
	d := NewConcurrentDispatcher(nil)

	for i := 0; i < b.N; i++ {
		d.Subscribe(e.Name, DummyHandler{})
	}

	//initHandlers(b.N, &d, e)

	d.Publish(e)
}

func initHandlers(n int, d Dispatcher, e Event) {
	for i := 0; i < n; i++ {
		d.Subscribe(e.Name, LogHandler{Prefix: strconv.Itoa(i)})
	}
}
