package event

import "log"

type SimpleDispatcher struct {
	handlers map[string][]Handler
}

func NewSimpleDispatcher() SimpleDispatcher {
	return SimpleDispatcher{
		handlers: make(map[string][]Handler),
	}
}

// Publish - publishes an event and returns a slice containing all the errors returned by the handlers
func (d *SimpleDispatcher) Publish(e Event) []error {
	log.Printf("Dispatch event %s\n", e.Name)

	var errors []error

	for _, handler := range d.getHandlers(e.Name) {
		err := handler.Handle(e)

		if err != nil {
			errors = append(errors, err)
		}
	}

	return errors
}

func (d *SimpleDispatcher) Subscribe(eventName string, h Handler) int {
	if d.handlers == nil {
		panic("Handlers are not initialised. SimpleDispatcher should be initialised through the NewSimpleDispatcher constructor.")
	}

	priority := len(d.handlers[eventName])

	d.handlers[eventName] = append(d.handlers[eventName], h)

	return priority
}

func (d *SimpleDispatcher) getHandlers(eventName string) []Handler {
	return d.handlers[eventName]
}
