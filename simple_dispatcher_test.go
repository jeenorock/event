package event

import (
	"testing"
)

func TestSimpleDispatcher_SubscribePanicsIfHandlersNotInitialized(t *testing.T) {
	defer func() {
		r := recover()
		if r == nil {
			t.Errorf("The code did not panic")
		} else if r != "Handlers are not initialised. SimpleDispatcher should be initialised through the new keyword." {
			t.Errorf("Unexpected panic: %s", r)
		}
	}()

	d := SimpleDispatcher{}
	h := LogHandler{}
	e := Event{Name: "test"}
	d.Subscribe(e.Name, h)
}

func TestSimpleDispatcher_Subscribe(t *testing.T) {
	e := Event{Name: "test"}
	d := NewSimpleDispatcher()

	p := d.Subscribe(e.Name, LogHandler{})

	if p != 0 {
		t.Errorf("Expected priority was 0, %d returned", p)
	}

	p = d.Subscribe(e.Name, LogHandler{})

	if p != 1 {
		t.Errorf("Expected priority was 1, %d returned", p)
	}

	if d.handlers[e.Name] == nil {
		t.Errorf("No handler found for event %s", e.Name)
	}

	if len(d.handlers[e.Name]) != 2 {
		t.Errorf("Expected 2 handlers for event %s, %d found", e.Name, len(d.handlers[e.Name]))
	}
}

func TestSimpleDispatcher_Publish(t *testing.T) {
	e := Event{Name: "test"}
	d := NewSimpleDispatcher()
	h := LogHandler{}
	h2 := LogHandler{}

	d.Subscribe(e.Name, h)
	d.Subscribe(e.Name, h2)
	d.Publish(e)
}
