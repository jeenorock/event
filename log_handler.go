package event

import "log"

type LogHandler struct {
	Prefix string
}

func (h LogHandler) Handle(e Event) error {
	log.Printf("[%s] Event: %s | Payload: %s", h.Prefix, e.Name, e.Payload)
	return nil
}
