package event

import (
	"log"
	"time"
)

type TimeoutHandler struct {
	Prefix  string
	Timeout time.Duration
}

func (h TimeoutHandler) Handle(e Event) error {
	log.Printf("[%s] Timeout: %v", h.Prefix, h.Timeout)
	time.Sleep(h.Timeout)
	log.Printf("[%s] Done", h.Prefix)

	return nil
}
