package event

import (
	"sync"
)

type BroadcastDispatcher struct {
	dispatcher SimpleDispatcher
}

func NewBroadcastDispatcher() BroadcastDispatcher {
	return BroadcastDispatcher{
		dispatcher: NewSimpleDispatcher(),
	}
}

func (d *BroadcastDispatcher) Publish(e Event) []error {
	var errors []error
	handlers := d.dispatcher.getHandlers(e.Name)
	eventSync := sync.NewCond(&sync.Mutex{})
	errChan := make(chan error)
	defer close(errChan)

	var publish sync.WaitGroup
	running := &sync.WaitGroup{}

	for _, handler := range handlers {
		decorator := broadcastHandlerDecorator{
			handler:       handler,
			runningStatus: running,
		}
		running.Add(1)
		publish.Add(1)

		go func(c *sync.Cond, h Handler) {
			publish.Done()
			c.L.Lock()
			defer c.L.Unlock()
			c.Wait()
			errChan <- decorator.Handle(e)
		}(eventSync, decorator)

		publish.Wait()
	}

	running.Add(1)

	go func(activeHandlers int) {
		for {
			select {
			case ret := <-errChan:
				activeHandlers--
				if ret != nil {
					errors = append(errors, ret)
				}
			}

			if activeHandlers <= 0 {
				break
			}
		}
		running.Done()
	}(len(handlers))

	eventSync.Broadcast()
	running.Wait()

	return errors
}

func (d *BroadcastDispatcher) Subscribe(eventName string, s Handler) int {
	_ = d.dispatcher.Subscribe(eventName, s)
	// there is no priority, the order of execution will be undeterministic based on the go routine scheduler logic
	return 0
}

type broadcastHandlerDecorator struct {
	handler       Handler
	runningStatus *sync.WaitGroup
}

func (h broadcastHandlerDecorator) Handle(e Event) error {
	err := h.handler.Handle(e)
	h.runningStatus.Done()
	return err
}
