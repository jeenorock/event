package event

type Dispatcher interface {
	// Publish - publishes an event and returns a slice containing all the errors returned by the handlers
	Publish(e Event) []error
	// Subscribe - subscribes a subscriber to an event and returns its priority
	Subscribe(eventName string, s Handler) int
}
