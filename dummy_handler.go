package event

type DummyHandler struct{}

func (d DummyHandler) Handle(e Event) error {
	return nil
}
