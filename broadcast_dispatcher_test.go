package event

import "testing"

func TestBroadcastDispatcher_Publish(t *testing.T) {
	e := Event{Name: "test", Payload: "Some text"}
	d := NewBroadcastDispatcher()
	initHandlers(10, &d, e)
	d.Publish(e)
}

func BenchmarkBroadcastDispatcher_Publish(b *testing.B) {
	e := Event{Name: "test"}
	d := NewBroadcastDispatcher()

	for i := 0; i < b.N; i++ {
		d.Subscribe(e.Name, DummyHandler{})
	}

	//initHandlers(b.N, &d, e)

	d.Publish(e)
}
